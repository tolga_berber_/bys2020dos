TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle

SOURCES += \
        GirisEkrani/bysgirisekrani.cpp \
        GirisEkrani/bysogrencigirisekrani.cpp \
        Menuler/bysanamenu.cpp \
        Menuler/bysogrencimenusu.cpp \
        bysmenuekrani.cpp \
        main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../VeriKatmani/release/ -lVeriKatmani
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../VeriKatmani/debug/ -lVeriKatmani
else:unix: LIBS += -L$$OUT_PWD/../VeriKatmani/ -lVeriKatmani

INCLUDEPATH += $$PWD/../VeriKatmani
DEPENDPATH += $$PWD/../VeriKatmani

HEADERS += \
    GirisEkrani/bysgirisekrani.h \
    GirisEkrani/bysogrencigirisekrani.h \
    Menuler/bysanamenu.h \
    Menuler/bysogrencimenusu.h \
    bysmenuekrani.h
