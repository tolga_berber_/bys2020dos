#include "bysogrencigirisekrani.h"

#include <bysvt.h>

BysOgrenciGirisEkrani::BysOgrenciGirisEkrani()
{
    
}

void BysOgrenciGirisEkrani::calistir()
{
    std::string okulno, tckimlik, adi, soyadi, eposta;
    int kayitYili;

    veriGir("Okul Numarası Giriniz :", okulno);
    veriGir("TCKimlik Numarası Giriniz :", tckimlik);
    veriGir("Öğrenci Adını Giriniz :", adi);
    veriGir("Öğrenci Soyadını Giriniz :", soyadi);
    veriGir("Öğrenci E-Postasını Giriniz :", eposta);
    veriGir("Kayıt Yılını :", kayitYili);

    auto ogrenci = BYSVT::db().ogrenciler().yeni();
    auto profil = BYSVT::db().profiller().yeni();

    ogrenci->setOkulNo(okulno);
    ogrenci->setKayitYili(kayitYili);
    profil->setAd(adi);
    profil->setSoyad(soyadi);
    profil->setTCKimlik(tckimlik);
    profil->setEposta(eposta);

    BYSVT::db().ogrenciler().ekle(ogrenci);
    BYSVT::db().profiller().ekle(profil);

    ogrenci->setProfilId(profil->id());
    profil->setOgrenciId(ogrenci->id());
}
