#ifndef BYSOGRENCIGIRISEKRANI_H
#define BYSOGRENCIGIRISEKRANI_H

#include "bysgirisekrani.h"

class BysOgrenciGirisEkrani : public BYSGirisEkrani
{
public:
    BysOgrenciGirisEkrani();

    virtual void calistir() override;
};

#endif // BYSOGRENCIGIRISEKRANI_H
