#ifndef BYSGIRISEKRANI_H
#define BYSGIRISEKRANI_H

#include <iostream>
#include <string>

class BYSGirisEkrani
{
protected:
    template<class T>
    void veriGir(std::string mesaj, T &degisken)
    {
        std::cout << mesaj << " ";
        std::cin >> degisken;
    }

public:
    BYSGirisEkrani();

    virtual void calistir() = 0;
};

#endif // BYSGIRISEKRANI_H
