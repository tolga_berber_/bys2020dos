#include "bysmenuekrani.h"

#include <iostream>

BYSMenuEkrani::BYSMenuEkrani(std::string baslik, BYSMenuEkrani *ustMenu)
    : _baslik(baslik), _ustMenu(ustMenu)
{}

void BYSMenuEkrani::menuCalistir(MenuElemanlari menuElemanlari)
{
    while (true) {
        std::cout << std::endl << _baslik << std::endl;
        unsigned long long i;
        for (i = 0; i < menuElemanlari.size(); i++) {
            std::cout << "[" << i + 1 << "] " << menuElemanlari[i].first << std::endl;
        }

        if (_ustMenu == nullptr) {
            std::cout << "[" << i + 1 << "] ÇIKIŞ" << std::endl;
        } else {
            std::cout << "[" << i + 1 << "] ÖNCEKİ MENÜ" << std::endl;
        }

        std::cout << "SEÇİMİNİZ : ";

        unsigned long long secim;
        std::cin >> secim;

        if (secim == menuElemanlari.size() + 1) {
            break;
        } else if (secim > 0 && secim <= menuElemanlari.size()) {
            menuElemanlari[secim - 1].second();
        } else {
            std::cout << "HATALI SEÇİM YENİDEN DENEYİN" << std::endl;
        }
    }
}
