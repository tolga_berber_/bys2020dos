#ifndef BYSMENUEKRANI_H
#define BYSMENUEKRANI_H

#include <functional>
#include <string>
#include <utility>
#include <vector>

class BYSMenuEkrani
{
public:
    typedef std::function<void()> MenuFonksiyonu;
    typedef std::pair<std::string, MenuFonksiyonu> MenuElemani;
    typedef std::vector<MenuElemani> MenuElemanlari;

protected:
    std::string _baslik;

    void menuCalistir(MenuElemanlari menuElemanlari);

    BYSMenuEkrani *_ustMenu;

public:
    BYSMenuEkrani(std::string baslik, BYSMenuEkrani *ustMenu = nullptr);

    virtual void calistir() = 0;
};

#endif // BYSMENUEKRANI_H
