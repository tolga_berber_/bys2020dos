#include "bysanamenu.h"

#include <iostream>

#include "bysogrencimenusu.h"

BYSAnaMenu::BYSAnaMenu() : BYSMenuEkrani("ANA MENÜ") {}

void BYSAnaMenu::calistir()
{
    MenuElemanlari menu = {{"ÖĞRENCİ MENÜSÜ", [this]() {
                                BYSOgrenciMenusu menu("ÖĞRENCİ MENÜSÜ", this);
                                menu.calistir();
                            }}};

    this->menuCalistir(menu);
}
