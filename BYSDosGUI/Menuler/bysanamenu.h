#ifndef BYSANAMENU_H
#define BYSANAMENU_H

#include <bysmenuekrani.h>

class BYSAnaMenu : public BYSMenuEkrani
{
public:
    BYSAnaMenu();

    virtual void calistir() override;
};

#endif // BYSANAMENU_H
