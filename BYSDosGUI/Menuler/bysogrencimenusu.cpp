#include "bysogrencimenusu.h"

#include <GirisEkrani/bysogrencigirisekrani.h>

BYSOgrenciMenusu::BYSOgrenciMenusu(std::string baslik, BYSMenuEkrani *ustMenu)
    : BYSMenuEkrani(baslik, ustMenu)
{}

void BYSOgrenciMenusu::calistir()
{
    MenuElemanlari menu = {{"ÖĞRENCİ GİRİŞİ",
                            []() {
                                BysOgrenciGirisEkrani ekran;
                                ekran.calistir();
                            }},
                           {"ÖĞRENCİ DÜZELTME", []() {}},
                           {"ÖĞRENCİ SİLME", []() {}},
                           {"ÖĞRENCİ ARAMA", []() {}}};

    this->menuCalistir(menu);
}
