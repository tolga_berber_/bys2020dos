#ifndef BYSOGRENCIMENUSU_H
#define BYSOGRENCIMENUSU_H

#include <bysmenuekrani.h>

class BYSOgrenciMenusu : public BYSMenuEkrani
{
public:
    BYSOgrenciMenusu(std::string baslik, BYSMenuEkrani *ustMenu = nullptr);

    virtual void calistir() override;
};

#endif // BYSOGRENCIMENUSU_H
