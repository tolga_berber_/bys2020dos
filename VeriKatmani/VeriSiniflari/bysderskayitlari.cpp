#include "bysderskayitlari.h"

BYSDersKayitlari::Pointer BYSDersKayitlari::yeni()
{
    return std::make_shared<BYSDersKayitlari>();
}

IdTuru BYSDersKayitlari::id() const
{
    return _id;
}

void BYSDersKayitlari::setId(const IdTuru &id)
{
    _id = id;
}

BYSDersKayitlari::BYSDersKayitlari()
{
    _id = 0;
}

BYSDersKayitlari::BYSDersKayitlari(const BYSDersKayitlari &diger)
{
    _id = diger._id;
}
