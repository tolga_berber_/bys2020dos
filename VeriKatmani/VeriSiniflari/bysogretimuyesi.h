#ifndef BYSOGRETIMUYESI_H
#define BYSOGRETIMUYESI_H

#include <VeriKatmani_global.h>
#include <VeriTuruTanimlari.h>
#include <memory>

class VERIKATMANI_EXPORT BYSOgretimUyesi
{
public:
    typedef std::shared_ptr<BYSOgretimUyesi> Pointer;

    static Pointer yeni();

private:
    IdTuru _id;
    Metin _sicilNo;
    Tarih _baslangic;

public:
    BYSOgretimUyesi();
    BYSOgretimUyesi(const BYSOgretimUyesi &diger);

    IdTuru id() const;
    void setId(const IdTuru &id);
    Metin sicilNo() const;
    void setSicilNo(const Metin &sicilNo);
    Tarih baslangic() const;
    void setBaslangic(const Tarih &baslangic);
};

#endif // BYSOGRETIMUYESI_H
