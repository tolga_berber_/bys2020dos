#ifndef BYSDERSNOTLANDIRMA_H
#define BYSDERSNOTLANDIRMA_H

#include <VeriKatmani_global.h>
#include <VeriTuruTanimlari.h>
#include <memory>

class VERIKATMANI_EXPORT BYSDersNotlandirma
{
public:
    typedef std::shared_ptr<BYSDersNotlandirma> Pointer;

    static Pointer yeni();

private:
    IdTuru _id;
    Metin _ad;
    ReelSayi _agirlik;

public:
    BYSDersNotlandirma();
    BYSDersNotlandirma(const BYSDersNotlandirma &diger);

    IdTuru id() const;
    void setId(const IdTuru &id);
    Metin ad() const;
    void setAd(const Metin &ad);
    ReelSayi agirlik() const;
    void setAgirlik(const ReelSayi &agirlik);
};

#endif // BYSDERSNOTLANDIRMA_H
