#ifndef BYSOGRENCI_H
#define BYSOGRENCI_H

#include <VeriKatmani_global.h>
#include <VeriTuruTanimlari.h>
#include <memory>

class BYSProfil;
typedef std::shared_ptr<BYSProfil> BYSProfilPointer;

class VERIKATMANI_EXPORT BYSOgrenci
{
public:
    typedef std::shared_ptr<BYSOgrenci> Pointer;

    static Pointer yeni();

private:
    IdTuru _id;
    Metin _okulNo;
    Tamsayi _kayitYili;

    // İlişkiler
    IdTuru _profilId;

public:
    BYSOgrenci();
    BYSOgrenci(const BYSOgrenci &diger);

    IdTuru id() const;
    void setId(const IdTuru &id);

    Metin okulNo() const;
    void setOkulNo(const Metin &okulNo);

    Tamsayi kayitYili() const;
    void setKayitYili(const Tamsayi &kayitYili);

    // İlişki fonksiyonları
    IdTuru profilId() const;
    void setProfilId(const IdTuru &profilId);

    BYSProfilPointer profil();
};

#endif // BYSOGRENCI_H
