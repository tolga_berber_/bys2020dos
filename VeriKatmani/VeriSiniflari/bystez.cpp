#include "bystez.h"

BYSTez::Pointer BYSTez::yeni()
{
    return std::make_shared<BYSTez>();
}

Metin BYSTez::ozet() const
{
    return _ozet;
}

void BYSTez::setOzet(const Metin &ozet)
{
    _ozet = ozet;
}

Metin BYSTez::ad() const
{
    return _ad;
}

void BYSTez::setAd(const Metin &ad)
{
    _ad = ad;
}

IdTuru BYSTez::id() const
{
    return _id;
}

void BYSTez::setId(const IdTuru &id)
{
    _id = id;
}

BYSTez::BYSTez()
{
    _id = 0;
    _ad = "";
    _ozet = "";
}

BYSTez::BYSTez(const BYSTez &diger)
{
    _id = diger._id;
    _ad = diger._ad;
    _ozet = diger._ozet;
}
