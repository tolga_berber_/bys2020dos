#include "bysogrenci.h"

#include <bysvt.h>

IdTuru BYSOgrenci::id() const
{
    return _id;
}

void BYSOgrenci::setId(const IdTuru &id)
{
    _id = id;
}

Metin BYSOgrenci::okulNo() const
{
    return _okulNo;
}

void BYSOgrenci::setOkulNo(const Metin &okulNo)
{
    _okulNo = okulNo;
}

Tamsayi BYSOgrenci::kayitYili() const
{
    return _kayitYili;
}

void BYSOgrenci::setKayitYili(const Tamsayi &kayitYili)
{
    _kayitYili = kayitYili;
}

BYSOgrenci::Pointer BYSOgrenci::yeni()
{
    return std::make_shared<BYSOgrenci>();
}

IdTuru BYSOgrenci::profilId() const
{
    return _profilId;
}

void BYSOgrenci::setProfilId(const IdTuru &profilId)
{
    _profilId = profilId;
}

BYSProfilPointer BYSOgrenci::profil()
{
    auto veri = BYSVT::db().profiller().birinciyiBul(
        [this](BYSProfilPointer p) { return p->id() == this->_profilId; });

    return veri;
}

BYSOgrenci::BYSOgrenci()
{
    _id = 0;
    _okulNo = "";
    _kayitYili = 0;
    _profilId = 0;
}

BYSOgrenci::BYSOgrenci(const BYSOgrenci &diger)
{
    _id = diger._id;
    _okulNo = diger._okulNo;
    _kayitYili = diger._kayitYili;
    _profilId = diger._profilId;
}
