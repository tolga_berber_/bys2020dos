#include "bysprofil.h"

#include <bysvt.h>

IdTuru BYSProfil::id() const
{
    return _id;
}

void BYSProfil::setId(const IdTuru &id)
{
    _id = id;
}

Metin BYSProfil::TCKimlik() const
{
    return _TCKimlik;
}

void BYSProfil::setTCKimlik(const Metin &TCKimlik)
{
    _TCKimlik = TCKimlik;
}

Metin BYSProfil::ad() const
{
    return _ad;
}

void BYSProfil::setAd(const Metin &ad)
{
    _ad = ad;
}

Metin BYSProfil::soyad() const
{
    return _soyad;
}

void BYSProfil::setSoyad(const Metin &soyad)
{
    _soyad = soyad;
}

Metin BYSProfil::eposta() const
{
    return _eposta;
}

void BYSProfil::setEposta(const Metin &eposta)
{
    _eposta = eposta;
}

BYSProfil::Pointer BYSProfil::yeni()
{
    return std::make_shared<BYSProfil>();
}

IdTuru BYSProfil::ogrenciId() const
{
    return _ogrenciId;
}

void BYSProfil::setOgrenciId(const IdTuru &ogrenciId)
{
    _ogrenciId = ogrenciId;
}

BYSOgrenciPointer BYSProfil::ogrenci()
{
    auto veri = BYSVT::db().ogrenciler().birinciyiBul(
        [this](BYSOgrenciPointer p) { return p->id() == this->_ogrenciId; });

    return veri;
}

BYSProfil::BYSProfil()
{
    _id = 0;
    _TCKimlik = "";
    _ad = "";
    _soyad = "";
    _eposta = "";
    _ogrenciId = 0;
}

BYSProfil::BYSProfil(const BYSProfil &other)
{
    _id = other._id;
    _TCKimlik = other._TCKimlik;
    _ad = other._ad;
    _soyad = other._soyad;
    _eposta = other._eposta;
    _ogrenciId = other._ogrenciId;
}
