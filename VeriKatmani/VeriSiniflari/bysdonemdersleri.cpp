#include "bysdonemdersleri.h"

BYSDonemDersleri::Pointer BYSDonemDersleri::yeni()
{
    return std::make_shared<BYSDonemDersleri>();
}

IdTuru BYSDonemDersleri::id() const
{
    return _id;
}

void BYSDonemDersleri::setId(const IdTuru &id)
{
    _id = id;
}

BYSDonemDersleri::BYSDonemDersleri()
{
    _id = 0;
}

BYSDonemDersleri::BYSDonemDersleri(const BYSDonemDersleri &diger)
{
    _id = diger._id;
}
