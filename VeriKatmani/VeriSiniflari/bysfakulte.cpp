#include "bysfakulte.h"

IdTuru BYSFakulte::id() const
{
    return _id;
}

void BYSFakulte::setId(const IdTuru &id)
{
    _id = id;
}

Metin BYSFakulte::adi() const
{
    return _adi;
}

void BYSFakulte::setAdi(const Metin &adi)
{
    _adi = adi;
}

BYSFakulte::Pointer BYSFakulte::yeni()
{
    return std::make_shared<BYSFakulte>();
}

BYSFakulte::BYSFakulte()
{
    _id = 0;
    _adi = "";
}

BYSFakulte::BYSFakulte(const BYSFakulte &diger)
{
    _id = diger._id;
    _adi = diger._adi;
}
