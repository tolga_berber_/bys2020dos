#include "bysdersler.h"

BYSDersler::Pointer BYSDersler::yeni()
{
    return std::make_shared<BYSDersler>();
}

Tamsayi BYSDersler::tur() const
{
    return _tur;
}

void BYSDersler::setTur(const Tamsayi &tur)
{
    _tur = tur;
}

Tamsayi BYSDersler::yariYil() const
{
    return _yariYil;
}

void BYSDersler::setYariYil(const Tamsayi &yariYil)
{
    _yariYil = yariYil;
}

Metin BYSDersler::icerik() const
{
    return _icerik;
}

void BYSDersler::setIcerik(const Metin &icerik)
{
    _icerik = icerik;
}

Metin BYSDersler::dersAdi() const
{
    return _dersAdi;
}

void BYSDersler::setDersAdi(const Metin &dersAdi)
{
    _dersAdi = dersAdi;
}

Metin BYSDersler::dersKodu() const
{
    return _dersKodu;
}

void BYSDersler::setDersKodu(const Metin &dersKodu)
{
    _dersKodu = dersKodu;
}

IdTuru BYSDersler::id() const
{
    return _id;
}

void BYSDersler::setId(const IdTuru &id)
{
    _id = id;
}

BYSDersler::BYSDersler()
{
    _id = 0;
    _tur = 0;
    _yariYil = 0;
    _dersKodu = "";
    _dersAdi = "";
    _icerik = "";
}

BYSDersler::BYSDersler(const BYSDersler &diger)
{
    _id = diger._id;
    _tur = diger._tur;
    _yariYil = diger._yariYil;
    _dersKodu = diger._dersKodu;
    _dersAdi = diger._dersAdi;
    _icerik = diger._icerik;
}
