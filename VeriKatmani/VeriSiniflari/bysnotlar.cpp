#include "bysnotlar.h"

BYSNotlar::Pointer BYSNotlar::yeni()
{
    return std::make_shared<BYSNotlar>();
}

ReelSayi BYSNotlar::alinanNot() const
{
    return _alinanNot;
}

void BYSNotlar::setAlinanNot(const ReelSayi &alinanNot)
{
    _alinanNot = alinanNot;
}

IdTuru BYSNotlar::id() const
{
    return _id;
}

void BYSNotlar::setId(const IdTuru &id)
{
    _id = id;
}

BYSNotlar::BYSNotlar()
{
    _id = 0;
    _alinanNot = 0.0;
}

BYSNotlar::BYSNotlar(const BYSNotlar &diger)
{
    _id = diger._id;
    _alinanNot = diger._alinanNot;
}
