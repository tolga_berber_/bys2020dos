#include "bysdonem.h"

BYSDonem::Pointer BYSDonem::yeni()
{
    return std::make_shared<BYSDonem>();
}

Tamsayi BYSDonem::yariYil() const
{
    return _yariYil;
}

void BYSDonem::setYariYil(const Tamsayi &yariYil)
{
    _yariYil = yariYil;
}

Tamsayi BYSDonem::yil() const
{
    return _yil;
}

void BYSDonem::setYil(const Tamsayi &yil)
{
    _yil = yil;
}

IdTuru BYSDonem::id() const
{
    return _id;
}

void BYSDonem::setId(const IdTuru &id)
{
    _id = id;
}

BYSDonem::BYSDonem()
{
    _id = 0;
    _yil = 0;
    _yariYil = 0;
}

BYSDonem::BYSDonem(const BYSDonem &diger)
{
    _id = diger._id;
    _yil = diger._yil;
    _yariYil = diger._yariYil;
}
