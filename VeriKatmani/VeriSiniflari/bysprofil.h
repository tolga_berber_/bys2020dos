#ifndef BYSPROFIL_H
#define BYSPROFIL_H

#include <VeriKatmani_global.h>
#include <VeriTuruTanimlari.h>
#include <memory>

class BYSOgrenci;
typedef std::shared_ptr<BYSOgrenci> BYSOgrenciPointer;

class VERIKATMANI_EXPORT BYSProfil
{
public:
    typedef std::shared_ptr<BYSProfil> Pointer;

    static Pointer yeni();

private:
    IdTuru _id;
    Metin _TCKimlik;
    Metin _ad;
    Metin _soyad;
    Metin _eposta;

    // İlişkiler
    IdTuru _ogrenciId;

public:
    BYSProfil();
    BYSProfil(const BYSProfil &other);

    IdTuru id() const;
    void setId(const IdTuru &id);

    Metin TCKimlik() const;
    void setTCKimlik(const Metin &TCKimlik);

    Metin ad() const;
    void setAd(const Metin &ad);

    Metin soyad() const;
    void setSoyad(const Metin &soyad);

    Metin eposta() const;
    void setEposta(const Metin &eposta);

    // İlişki Fonksiyonları
    IdTuru ogrenciId() const;
    void setOgrenciId(const IdTuru &ogrenciId);

    BYSOgrenciPointer ogrenci();
};

#endif // BYSPROFIL_H
