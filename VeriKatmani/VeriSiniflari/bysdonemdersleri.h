#ifndef BYSDONEMDERSLERI_H
#define BYSDONEMDERSLERI_H

#include <VeriKatmani_global.h>
#include <VeriTuruTanimlari.h>
#include <memory>

class VERIKATMANI_EXPORT BYSDonemDersleri
{
public:
    typedef std::shared_ptr<BYSDonemDersleri> Pointer;

    static Pointer yeni();

private:
    IdTuru _id;

public:
    BYSDonemDersleri();
    BYSDonemDersleri(const BYSDonemDersleri &diger);

    IdTuru id() const;
    void setId(const IdTuru &id);
};

#endif // BYSDONEMDERSLERI_H
