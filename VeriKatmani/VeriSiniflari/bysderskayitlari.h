#ifndef BYSDERSKAYITLARI_H
#define BYSDERSKAYITLARI_H

#include <VeriKatmani_global.h>
#include <VeriTuruTanimlari.h>
#include <memory>

class VERIKATMANI_EXPORT BYSDersKayitlari
{
public:
    typedef std::shared_ptr<BYSDersKayitlari> Pointer;

    static Pointer yeni();

private:
    IdTuru _id;

public:
    BYSDersKayitlari();
    BYSDersKayitlari(const BYSDersKayitlari &diger);

    IdTuru id() const;
    void setId(const IdTuru &id);
};

#endif // BYSDERSKAYITLARI_H
