#include "bysogretimuyesi.h"

BYSOgretimUyesi::Pointer BYSOgretimUyesi::yeni()
{
    return std::make_shared<BYSOgretimUyesi>();
}

Tarih BYSOgretimUyesi::baslangic() const
{
    return _baslangic;
}

void BYSOgretimUyesi::setBaslangic(const Tarih &baslangic)
{
    _baslangic = baslangic;
}

Metin BYSOgretimUyesi::sicilNo() const
{
    return _sicilNo;
}

void BYSOgretimUyesi::setSicilNo(const Metin &sicilNo)
{
    _sicilNo = sicilNo;
}

IdTuru BYSOgretimUyesi::id() const
{
    return _id;
}

void BYSOgretimUyesi::setId(const IdTuru &id)
{
    _id = id;
}

BYSOgretimUyesi::BYSOgretimUyesi()
{
    _id = 0;
    _sicilNo = "";
    _baslangic = "1900-01-01";
}

BYSOgretimUyesi::BYSOgretimUyesi(const BYSOgretimUyesi &diger)
{
    _id = diger._id;
    _sicilNo = diger._sicilNo;
    _baslangic = diger._baslangic;
}
