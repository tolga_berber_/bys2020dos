#ifndef BYSDONEM_H
#define BYSDONEM_H

#include <VeriKatmani_global.h>
#include <VeriTuruTanimlari.h>
#include <memory>

class VERIKATMANI_EXPORT BYSDonem
{
public:
    typedef std::shared_ptr<BYSDonem> Pointer;

    static Pointer yeni();

private:
    IdTuru _id;
    Tamsayi _yil;
    Tamsayi _yariYil;

public:
    BYSDonem();
    BYSDonem(const BYSDonem &diger);

    IdTuru id() const;
    void setId(const IdTuru &id);
    Tamsayi yil() const;
    void setYil(const Tamsayi &yil);
    Tamsayi yariYil() const;
    void setYariYil(const Tamsayi &yariYil);
};

#endif // BYSDONEM_H
