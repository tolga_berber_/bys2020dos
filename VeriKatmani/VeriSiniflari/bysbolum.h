#ifndef BYSBOLUM_H
#define BYSBOLUM_H

#include <VeriKatmani_global.h>
#include <VeriTuruTanimlari.h>
#include <memory>

class VERIKATMANI_EXPORT BYSBolum
{
public:
    typedef std::shared_ptr<BYSBolum> Pointer;

    static Pointer yeni();

private:
    IdTuru _id;
    Metin _adi;

public:
    BYSBolum();
    BYSBolum(const BYSBolum &diger);

    IdTuru id() const;
    void setId(const IdTuru &id);
    Metin adi() const;
    void setAdi(const Metin &adi);
};

#endif // BYSBOLUM_H
