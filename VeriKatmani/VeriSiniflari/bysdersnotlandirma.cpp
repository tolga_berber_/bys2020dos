#include "bysdersnotlandirma.h"

BYSDersNotlandirma::Pointer BYSDersNotlandirma::yeni()
{
    return std::make_shared<BYSDersNotlandirma>();
}

ReelSayi BYSDersNotlandirma::agirlik() const
{
    return _agirlik;
}

void BYSDersNotlandirma::setAgirlik(const ReelSayi &agirlik)
{
    _agirlik = agirlik;
}

Metin BYSDersNotlandirma::ad() const
{
    return _ad;
}

void BYSDersNotlandirma::setAd(const Metin &ad)
{
    _ad = ad;
}

IdTuru BYSDersNotlandirma::id() const
{
    return _id;
}

void BYSDersNotlandirma::setId(const IdTuru &id)
{
    _id = id;
}

BYSDersNotlandirma::BYSDersNotlandirma()
{
    _id = 0;
    _ad = "";
    _agirlik = 0.0;
}

BYSDersNotlandirma::BYSDersNotlandirma(const BYSDersNotlandirma &diger)
{
    _id = diger._id;
    _ad = diger._ad;
    _agirlik = diger._agirlik;
}
