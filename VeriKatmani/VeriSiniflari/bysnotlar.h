#ifndef BYSNOTLAR_H
#define BYSNOTLAR_H

#include <VeriKatmani_global.h>
#include <VeriTuruTanimlari.h>
#include <memory>

class VERIKATMANI_EXPORT BYSNotlar
{
public:
    typedef std::shared_ptr<BYSNotlar> Pointer;

    static Pointer yeni();

private:
    IdTuru _id;
    ReelSayi _alinanNot;

public:
    BYSNotlar();
    BYSNotlar(const BYSNotlar &diger);

    IdTuru id() const;
    void setId(const IdTuru &id);
    ReelSayi alinanNot() const;
    void setAlinanNot(const ReelSayi &alinanNot);
};

#endif // BYSNOTLAR_H
