#ifndef BYSDERSLER_H
#define BYSDERSLER_H

#include <VeriKatmani_global.h>
#include <VeriTuruTanimlari.h>
#include <memory>

class VERIKATMANI_EXPORT BYSDersler
{
public:
    typedef std::shared_ptr<BYSDersler> Pointer;

    static Pointer yeni();

private:
    IdTuru _id;
    Metin _dersKodu;
    Metin _dersAdi;
    Metin _icerik;
    Tamsayi _yariYil;
    Tamsayi _tur;

public:
    BYSDersler();
    BYSDersler(const BYSDersler &diger);

    IdTuru id() const;
    void setId(const IdTuru &id);
    Metin dersKodu() const;
    void setDersKodu(const Metin &dersKodu);
    Metin dersAdi() const;
    void setDersAdi(const Metin &dersAdi);
    Metin icerik() const;
    void setIcerik(const Metin &icerik);
    Tamsayi yariYil() const;
    void setYariYil(const Tamsayi &yariYil);
    Tamsayi tur() const;
    void setTur(const Tamsayi &tur);
};

#endif // BYSDERSLER_H
