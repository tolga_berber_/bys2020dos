#include "bysbolum.h"

IdTuru BYSBolum::id() const
{
    return _id;
}

void BYSBolum::setId(const IdTuru &id)
{
    _id = id;
}

Metin BYSBolum::adi() const
{
    return _adi;
}

void BYSBolum::setAdi(const Metin &adi)
{
    _adi = adi;
}

BYSBolum::Pointer BYSBolum::yeni()
{
    return std::make_shared<BYSBolum>();
}

BYSBolum::BYSBolum()
{
    _id = 0;
    _adi = "";
}

BYSBolum::BYSBolum(const BYSBolum &diger)
{
    _id = diger._id;
    _adi = diger._adi;
}
