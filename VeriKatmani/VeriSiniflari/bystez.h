#ifndef BYSTEZ_H
#define BYSTEZ_H

#include <VeriKatmani_global.h>
#include <VeriTuruTanimlari.h>
#include <memory>

class VERIKATMANI_EXPORT BYSTez
{
public:
    typedef std::shared_ptr<BYSTez> Pointer;

    static Pointer yeni();

private:
    IdTuru _id;
    Metin _ad;
    Metin _ozet;

public:
    BYSTez();
    BYSTez(const BYSTez &diger);

    IdTuru id() const;
    void setId(const IdTuru &id);
    Metin ad() const;
    void setAd(const Metin &ad);
    Metin ozet() const;
    void setOzet(const Metin &ozet);
};

#endif // BYSTEZ_H
