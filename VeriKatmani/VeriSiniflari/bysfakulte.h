#ifndef BYSFAKULTE_H
#define BYSFAKULTE_H

#include <VeriKatmani_global.h>
#include <VeriTuruTanimlari.h>
#include <memory>

class VERIKATMANI_EXPORT BYSFakulte
{
public:
    typedef std::shared_ptr<BYSFakulte> Pointer;

    static Pointer yeni();

private:
    IdTuru _id;
    Metin _adi;

public:
    BYSFakulte();
    BYSFakulte(const BYSFakulte &diger);

    IdTuru id() const;
    void setId(const IdTuru &id);
    Metin adi() const;
    void setAdi(const Metin &adi);
};

#endif // BYSFAKULTE_H
