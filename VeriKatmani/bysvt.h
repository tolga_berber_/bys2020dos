#ifndef BYSVT_H
#define BYSVT_H

#include <VeriYonetici/VeriYoneticisi.h>

#include <VeriSiniflari/bysbolum.h>
#include <VeriSiniflari/bysderskayitlari.h>
#include <VeriSiniflari/bysdersler.h>
#include <VeriSiniflari/bysdersnotlandirma.h>
#include <VeriSiniflari/bysdonem.h>
#include <VeriSiniflari/bysdonemdersleri.h>
#include <VeriSiniflari/bysfakulte.h>
#include <VeriSiniflari/bysnotlar.h>
#include <VeriSiniflari/bysogrenci.h>
#include <VeriSiniflari/bysogretimuyesi.h>
#include <VeriSiniflari/bysprofil.h>
#include <VeriSiniflari/bystez.h>

class VERIKATMANI_EXPORT BYSVT
{
private:
    BYSVT();

    VeriYoneticisi<BYSProfil> _profiller;
    VeriYoneticisi<BYSOgrenci> _ogrenciler;
    VeriYoneticisi<BYSFakulte> _fakulteler;
    VeriYoneticisi<BYSBolum> _bolumler;
    VeriYoneticisi<BYSDersler> _dersler;
    VeriYoneticisi<BYSDonem> _donemler;
    VeriYoneticisi<BYSOgretimUyesi> _ogretimUyeleri;
    VeriYoneticisi<BYSDonemDersleri> _donemDersleri;
    VeriYoneticisi<BYSDersKayitlari> _dersKayitlari;
    VeriYoneticisi<BYSDersNotlandirma> _dersNotlandirma;
    VeriYoneticisi<BYSTez> _tezler;
    VeriYoneticisi<BYSNotlar> _notlar;

public:
    static BYSVT &db();

    VeriYoneticisi<BYSProfil> &profiller();
    VeriYoneticisi<BYSOgrenci> &ogrenciler();
    VeriYoneticisi<BYSFakulte> &fakulteler();
    VeriYoneticisi<BYSBolum> &bolumler();
    VeriYoneticisi<BYSDersler> &dersler();
    VeriYoneticisi<BYSDonem> &donemler();
    VeriYoneticisi<BYSOgretimUyesi> &ogretimUyeleri();
    VeriYoneticisi<BYSDonemDersleri> &donemDersleri();
    VeriYoneticisi<BYSDersKayitlari> &dersKayitlari();
    VeriYoneticisi<BYSTez> &tezler();
    VeriYoneticisi<BYSNotlar> &notlar();
};

#endif // BYSVT_H
