#ifndef VERIYONETICISI_H
#define VERIYONETICISI_H

#include <VeriTuruTanimlari.h>
#include <functional>
#include <vector>

template<class T>
class VeriYoneticisi
{
public:
    typedef typename T::Pointer Veri;
    typedef std::vector<Veri> VeriListesi;
    typedef std::function<bool(Veri v)> FiltreFonksiyonu;
    typedef std::function<bool(Veri v, Veri v1)> KontrolFonksiyonu;
    typedef std::function<void(Veri v)> VeriDegistirmeFonksiyonu;
    typedef std::function<ReelSayi(const VeriListesi &v)> VeriOzetlemeFonksiyonu;

    static const KontrolFonksiyonu TumuDogru;
    static const KontrolFonksiyonu TumuYanlis;

    static const FiltreFonksiyonu TumuKabul;
    static const FiltreFonksiyonu TumuRed;

private:
    IdTuru _sonId;
    VeriListesi _veriler;

public:
    VeriYoneticisi() { _sonId = 0; }

    /**
     * @brief yeni Yeni Veri Oluşturur
     * 
     * Veri listesine eklenmeyen yeni bir nesne oluşturan fonksiyon.
     * 
     * @return Yeni oluşturulan nesne.
     */
    Veri yeni() { return T::yeni(); }

    /**
     * @brief ekle Veriyi Listeye Ekler
     * 
     * Veri listesine berlirtilen elemanı ekler. Ekleme yapılırken,
     * Filtre fonksiyonunu tum liste için çalıştırır ve tüm liste için 
     * false dönmesini bekler.
     * 
     * @param v Eklenecek Veri
     * @param f Kontrol Fonksiyonu (Tüm elemanlar için false dönmelidir, aksi halde ekleme yapılmaz.
     */
    void ekle(Veri v, KontrolFonksiyonu f = TumuYanlis)
    {
        for (auto e : _veriler) {
            if (f(e, v)) {
                return;
            }
        }
        v->setId(++_sonId);
        _veriler.push_back(v);
    }

    /**
     * @brief duzelt Veri Listesindeki Bir Elemanı Düzenler
     * 
     * Veri listesindeki bir elemanı değiştirmek için kullanılan fonksiyondur.
     * Eski değer listede yoksa herhangi bir işlem yapılmaz.
     * 
     * @param eski Değiştirilecek olan veri
     * @param yeni Yeni veri
     */
    void duzelt(Veri eski, Veri yeni)
    {
        for (auto &t : _veriler) {
            if (t == eski) {
                t = yeni;
                return;
            }
        }
    }

    /**
     * @brief sil Veri Listesinden Eleman Sil
     * 
     * İndisi belirtilen elemanı listeden siler
     * 
     * @param idx Silinecek olan elemanın indisi
     */
    void sil(IndexTuru idx) { _veriler.erase(_veriler.begin() + idx); }

    /**
     * @brief sil Belirtilen Elemanı Listeden Sil
     * 
     * Belirtilen elemanı listeden siler.
     * 
     * @param silinecek Silinecek olan eleman
     */
    void sil(Veri silinecek)
    {
        for (int i = 0; i < _veriler.size(); i++) {
            if (_veriler.at(i) == silinecek) {
                sil(i);
            }
        }
    }

    /**
     * @brief ara Veri Listesinde Arama Yapar
     * 
     * Veri listesinde arama yapar, belirtilen şarta uyan kayıtlardan yeni bir liste oluşturur.
     * 
     * @param f Filtre Fonksiyonu, her bir veri için çalıştırılır ve true döndüren elemanlar sonuç listesine eklenir.
     * @return Belirtilen filtre fonksiyonunu doğru olarak çalıştıran tüm veriler.
     */
    VeriListesi ara(FiltreFonksiyonu f)
    {
        VeriListesi sonuc;
        for (auto v : _veriler) {
            if (f(v)) {
                sonuc.push_back(v);
            }
        }

        return sonuc;
    }

    /**
     * @brief birinciyiBul Filtre Fonksiyonuna Uyan İlk Değeri Bulur
     * 
     * Veri listesinde filtre fonksiyonuna uyan ilk elemanı bulup geri döndürür. Eleman listede yoksa
     * nullptr döndürür.
     * 
     * @param f Uygulanacak Filtre Fonksiyonu
     * @return Filtre fonksiyonuna uyan ilk eleman veya nullptr
     */
    Veri birinciyiBul(FiltreFonksiyonu f)
    {
        for (auto v : _veriler) {
            if (f(v)) {
                return v;
            }
        }
        return nullptr;
    }

    /**
     * @brief sonuncuyuBul Filtre Fonksiyonuna Uyan İlk Değeri Bulur
     * 
     * Veri listesinde filtre fonksiyonuna uyan son elemanı bulup geri döndürür. Eleman listede yoksa
     * nullptr döndürür.
     * 
     * @param f Uygulanacak Filtre Fonksiyonu
     * @return Filtre fonksiyonuna uyan son eleman veya nullptr
     */
    Veri sonuncuyuBul(FiltreFonksiyonu f)
    {
        for (auto i = _veriler.rbegin(); i != _veriler.rend(); i++) {
            auto v = *i;
            if (f(v)) {
                return v;
            }
        }
        return nullptr;
    }

    /**
     * @brief ozetle Veri Özetlemesi Yapar
     * 
     * Verileri filtreden geçirip, özetleme fonksiyonunu çalıştırır.
     * 
     * @param f Ozetleme fonksiyonu
     * @param f1 Filtreleme fonksiyonu
     * @return Belirtilen filtreye uyan verilerin özeti
     */
    ReelSayi ozetle(VeriOzetlemeFonksiyonu f, FiltreFonksiyonu f1 = TumuKabul)
    {
        VeriListesi liste = ara(f1);
        return f(liste);
    }

    /**
     * @brief elemanSayisi Kayıtlı Eleman Sayısı
     * 
     * Saklanan elemanların sayısını verir.
     * 
     * @return Saklanan eleman sayısı
     */
    Tamsayi elemanSayisi() { return _veriler.size(); }
};

template<class T>
const typename VeriYoneticisi<T>::KontrolFonksiyonu VeriYoneticisi<T>::TumuDogru =
    [](Veri, Veri) { return true; };

template<class T>
const typename VeriYoneticisi<T>::KontrolFonksiyonu VeriYoneticisi<T>::TumuYanlis =
    [](Veri, Veri) { return false; };

template<class T>
const typename VeriYoneticisi<T>::FiltreFonksiyonu VeriYoneticisi<T>::TumuKabul =
    [](Veri) { return true; };

template<class T>
const typename VeriYoneticisi<T>::FiltreFonksiyonu VeriYoneticisi<T>::TumuRed =
    [](Veri) { return false; };

#endif // VERIYONETICISI_H
