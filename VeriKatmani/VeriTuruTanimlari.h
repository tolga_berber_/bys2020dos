#ifndef VERITURUTANIMLARI_H
#define VERITURUTANIMLARI_H

#include <string>

typedef std::string Metin;
typedef unsigned long long IdTuru;
typedef unsigned long long IsaretsizTamsayi;
typedef unsigned long long IndexTuru;
typedef long long Tamsayi;
typedef std::string Tarih;
typedef double ReelSayi;

#endif // VERITURUTANIMLARI_H
