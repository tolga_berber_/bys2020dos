#include "bysvt.h"

BYSVT::BYSVT() {}

VeriYoneticisi<BYSNotlar> &BYSVT::notlar()
{
    return _notlar;
}

VeriYoneticisi<BYSTez> &BYSVT::tezler()
{
    return _tezler;
}

VeriYoneticisi<BYSDersKayitlari> &BYSVT::dersKayitlari()
{
    return _dersKayitlari;
}

VeriYoneticisi<BYSDonemDersleri> &BYSVT::donemDersleri()
{
    return _donemDersleri;
}

VeriYoneticisi<BYSOgretimUyesi> &BYSVT::ogretimUyeleri()
{
    return _ogretimUyeleri;
}

VeriYoneticisi<BYSDonem> &BYSVT::donemler()
{
    return _donemler;
}

VeriYoneticisi<BYSDersler> &BYSVT::dersler()
{
    return _dersler;
}

VeriYoneticisi<BYSBolum> &BYSVT::bolumler()
{
    return _bolumler;
}

VeriYoneticisi<BYSFakulte> &BYSVT::fakulteler()
{
    return _fakulteler;
}

VeriYoneticisi<BYSOgrenci> &BYSVT::ogrenciler()
{
    return _ogrenciler;
}

VeriYoneticisi<BYSProfil> &BYSVT::profiller()
{
    return _profiller;
}

BYSVT &BYSVT::db()
{
    static BYSVT nesne;
    return nesne;
}
